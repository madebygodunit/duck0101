// DUDUDUCK TEST 01

// временные конфиги

// цвет внешнего кольца стартовой картинки
START_PIC_BORDER_COLOR_01 = 0xff7080;

// цвет внутреннего кольца стартовой картинки
START_PIC_BORDER_COLOR_02 = 0xffff72;

// цвет внешних шариков стартовой картинки
START_PIC_SPHERES_COLOR_01 = 0xb4ffff;

// цвет внутренних шариков стартовой картинки
START_PIC_SPHERES_COLOR_02 = 0xff7080;

// цвет задника стартовой картинки
START_PIC_BACK_COLOR = 0xffb2ff;

// номер главной картинки на стартовом экране
MAIN_PIC_NUM = 0;

// цвет названия игры
TOY_NAME_COLOR = 0xffff52;

// цвет обводки названия игры
TOY_NAME_BORDER_COLOR = 0x7c43bd;

// текст описания игры
DESCRIPTION_TEXT = [
  'Узнай, какая ты',
  'уточка!'
];

// создаем материалы для графики
const startPicBorderMaterial01 = new THREE.MeshLambertMaterial({ color: START_PIC_BORDER_COLOR_01 });
const startPicBorderMaterial02 = new THREE.MeshLambertMaterial({ color: START_PIC_BORDER_COLOR_02 });
const startPicSpheresMaterial01 = new THREE.MeshLambertMaterial({ color: START_PIC_SPHERES_COLOR_01 });
const startPicSpheresMaterial02 = new THREE.MeshLambertMaterial({ color: START_PIC_SPHERES_COLOR_02 });
const startPicBackMaterial = new THREE.MeshLambertMaterial({ color: START_PIC_BACK_COLOR });
const toyNameBorderMaterial = new THREE.MeshLambertMaterial({ color: TOY_NAME_BORDER_COLOR });
const toyNameMaterial = new THREE.MeshLambertMaterial({ color: TOY_NAME_COLOR });



const whiteBasicMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff });
const blackBasicMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 });




let finView, finY, touchDetect = false, activeParam, finResult = 0, view, playLine = 0, loadingCount = 20, geometry = [], shape = [], step = 0;
const green1TextMaterial = new THREE.MeshBasicMaterial({ color: 0x00ffd1 });
const green2TextMaterial = new THREE.MeshBasicMaterial({ color: 0xacfff0 });
const textBasicMaterial = new THREE.MeshBasicMaterial({ color: 0x3367a4 });
const backNormalMaterial = new THREE.MeshNormalMaterial();
const secondBackMaterial = new THREE.MeshBasicMaterial({ color: 0x300057, transparent: true, fog: false });
const pinkPhongMaterial = new THREE.MeshPhongMaterial({ color: 0xff7080, emissive: 0xe93e50 });
const pinkPurplePhongMaterial = new THREE.MeshPhongMaterial({ color: 0xe93e50, emissive: 0x181026 });
const purplePhongMaterial = new THREE.MeshPhongMaterial({ color: 0x7047b3 });






const renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true, powerPreference: "high-performance" });
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(46, window.innerWidth / window.innerHeight, 1, 120);
camera.position.z = 250;
const ambientLight = new THREE.AmbientLight(0xffffff, 0.6);
const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
scene.add(ambientLight, directionalLight);
directionalLight.position.set(-50, 30, 50);
scene.background = new THREE.Color(0xffffff);
//scene.fog = new THREE.Fog(0x1c0033, 40, 70);
window.addEventListener('resize', onWindowResize, false);
onWindowResize();
let secondBack = new THREE.Mesh(new THREE.PlaneBufferGeometry(700, 200), secondBackMaterial);
scene.add(secondBack);
let text_loading;
new THREE.FontLoader().load('fonts/BalsamiqSansBold.json', function(font) {
  descriptionFont = font;
  geometry[0] = new THREE.ShapeBufferGeometry(descriptionFont.generateShapes('Загрузка...', 0.14), 2);
  geometry[0].computeBoundingBox();
  let xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, 0, 240);
  text_loading = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  scene.add(text_loading);
  goLoad();
});
let pic = [];
function goLoad() {
  new THREE.FontLoader().load('fonts/RubikBoldItalic.json', function(font) {
    toyNameFont = font;
    loadingCount ? loadingCount-- : createStartScreen();
  });
  for (let i = 0; i < 20; i++) {
    loadPIC(`textures/pic${i}.png`).then(texture => {
      pic[i] = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
      loadingCount ? loadingCount-- : createStartScreen();
    });
  }
}
function loadPIC(url) {
  return new Promise(resolve => {
    new THREE.TextureLoader().load(url, resolve)
  })
}

// базовые переменные
const startMainPic = new THREE.Object3D();
const toyName = new THREE.Object3D();
const playButton = new THREE.Object3D();
const toyDescription = new THREE.Object3D();


// создаем графики для стартового экрана
function createStartScreen() {
  let geometry = [];
  let tempGeometry = [];
// создаем рамку для картинки стартового экрана
  startMainPic.part = [];
  startMainPic.part[0] = new THREE.Mesh(new THREE.TorusBufferGeometry(2, 0.13, 12, 96), startPicBorderMaterial01);
  startMainPic.part[1] = new THREE.Mesh(new THREE.TorusBufferGeometry(1.81, 0.06, 12, 96), startPicBorderMaterial02);
  geometry[0] = new THREE.SphereBufferGeometry(0.22, 32, 16);
  geometry[0].translate(0, 0.22, 0);
  for (let i = 2; i < 26; i++) {
    startMainPic.part[i] = new THREE.Mesh(geometry[0], startPicSpheresMaterial01);
    startMainPic.part[i].position.set(2 * Math.cos(Math.PI / 12 * i), 2 * Math.sin(Math.PI / 12 * i), 0.1);
    startMainPic.part[i].rotation.z = Math.PI / 12 * i - Math.PI / 2;
    startMainPic.part[i].scale.set(0, 0, 0);
  }
  geometry[1] = new THREE.CircleBufferGeometry(2, 24);
  startMainPic.part[26] = new THREE.Mesh(geometry[1], pic[MAIN_PIC_NUM]);
  startMainPic.part[26].position.z = 0.01;
  for (let i = 0; i < 24; i++) {
    tempGeometry[i] = new THREE.SphereBufferGeometry(0.08, 12, 8);
    tempGeometry[i].translate(1.75 * Math.cos(Math.PI / 12 * i), 1.75 * Math.sin(Math.PI / 12 * i), 0.05);
  }
  geometry[2] = new THREE.BufferGeometryUtils.mergeBufferGeometries(tempGeometry);
  clearTempGeometries();
  startMainPic.part[27] = new THREE.Mesh(geometry[2], startPicSpheresMaterial02);
  startMainPic.part[28] = new THREE.Mesh(geometry[1], startPicBackMaterial);
  startMainPic.part[28].rotation.x = Math.PI;
  startMainPic.part[28].position.z = -0.035;
  for (let i = 0; i < startMainPic.part.length; i++) {
    startMainPic.add(startMainPic.part[i]);
  }
  startMainPic.position.set(0, 1.5, 70);
  startMainPic.scale.set(0, 0, 0);




// создаем текст названия игры
  toyName.part = [];
  geometry[3] = new THREE.TextGeometry('Лалафанфан', {
    font: toyNameFont,
    size: 0.25,
    height: 0.05,
    curveSegments: 4,
    bevelEnabled: true,
    bevelThickness: 0.03,
    bevelSize: 0.03,
    bevelOffset: -0.005,
    bevelSegments: 8
  });
  geometry[3].computeBoundingBox();
  let xMid = -0.5 * (geometry[3].boundingBox.max.x - geometry[3].boundingBox.min.x);
  let yMid = -0.5 * (geometry[3].boundingBox.max.y - geometry[3].boundingBox.min.y);
  geometry[3].translate(xMid, yMid, 0.11);
  toyName.part[0] = new THREE.Mesh(geometry[3], toyNameMaterial);
  geometry[4] = new THREE.TextGeometry('Лалафанфан', {
    font: toyNameFont,
    size: 0.25,
    height: 0.03,
    curveSegments: 4,
    bevelEnabled: true,
    bevelThickness: 0.02,
    bevelSize: 0.04,
    bevelOffset: 0,
    bevelSegments: 4
  });
  geometry[4].computeBoundingBox();
  xMid = -0.5 * (geometry[4].boundingBox.max.x - geometry[4].boundingBox.min.x);
  yMid = -0.5 * (geometry[4].boundingBox.max.y - geometry[4].boundingBox.min.y);

  geometry[4].translate(xMid, yMid, 0.11);

  toyName.part[1] = new THREE.Mesh(geometry[4], toyNameBorderMaterial);
  

  toyName.add(toyName.part[0], toyName.part[1]);
  
  toyName.rotation.z = 0.2;
  
  
  toyName.position.set(0.02, 0, 75);
  
// создаем описание игры
  toyDescription.line = [];
  for (let i = 0; i < DESCRIPTION_TEXT.length; i++) {
    toyDescription.line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(descriptionFont.generateShapes(DESCRIPTION_TEXT[i], 0.15), 3), blackBasicMaterial);
    
    toyDescription.line[i].geometry.computeBoundingBox();
    xMid = -0.5 * (toyDescription.line[i].geometry.boundingBox.max.x - toyDescription.line[i].geometry.boundingBox.min.x);
    yMid = -0.5 * (toyDescription.line[i].geometry.boundingBox.max.y - toyDescription.line[i].geometry.boundingBox.min.y);
    toyDescription.line[i].geometry.translate(xMid, yMid - 0.21 * i, 0);
   
   
    toyDescription.add(toyDescription.line[i]);
  }
  
  toyDescription.position.set(0, -0.6, 75);
  
// создаем кнопку начала игры
  playButton.part = [];

  let shape = new THREE.Shape();
  shape.absarc(0.15, 0, 0.24, -Math.PI / 2, Math.PI / 2);
  shape.absarc(-0.15, 0, 0.24, Math.PI / 2, Math.PI * 1.5);

  geometry[5] = new THREE.ExtrudeBufferGeometry(shape, { steps: 1, depth: 0.04, curveSegments: 16, bevelEnabled: true, bevelThickness: 0.05, bevelSize: 0.24, bevelOffset: -0.24, bevelSegments: 16}, 4);
  geometry[5].translate(0, 0, 0);

  playButton.part[0] = new THREE.Mesh(geometry[5], toyNameBorderMaterial);

  shape = null;
  shape = new THREE.Shape()
  shape.absarc(0.11, 0, 0.06, -Math.PI / 3, Math.PI / 3);
  shape.absarc(-0.06, 0.09, 0.06, Math.PI / 3, Math.PI);
  shape.absarc(-0.06, -0.09, 0.06, Math.PI, Math.PI / 3 * 5);

  geometry[6] = new THREE.ExtrudeBufferGeometry(shape, { steps: 1, depth: 0, curveSegments: 16, bevelEnabled: true, bevelThickness: 0.06, bevelSize: 0.06, bevelOffset: -0.06, bevelSegments: 16}, 4);
  geometry[6].translate(0, 0, 0.07);
  playButton.part[1] = new THREE.Mesh(geometry[6], toyNameMaterial);


  shape = null;
  shape = new THREE.Shape()
  shape.absarc(0.15, 0, 0.26, -Math.PI / 2, Math.PI / 2);
  shape.absarc(-0.15, 0, 0.26, Math.PI / 2, Math.PI * 1.5);


  playButton.position.set(0, -1.5, 75);
  geometry[7] = new THREE.ExtrudeBufferGeometry(shape, { steps: 1, depth: 0, curveSegments: 16, bevelEnabled: true, bevelThickness: 0.02, bevelSize: 0.03, bevelOffset: 0, bevelSegments: 16}, 4);
  geometry[7].translate(0, 0, 0);

  playButton.part[2] = new THREE.Mesh(geometry[7], toyNameBorderMaterial);

  
  playButton.add(playButton.part[0], playButton.part[1], playButton.part[2]);

  
  
  
  
  //scene.add(startMainPic, toyName, playButton, toyDescription);
  scene.add(startMainPic)
 // gsap.to(startMainPic.rotation, { duration: 2, y: Math.PI * 0.2, ease: "none", repeat: -1, yoyo: true });

 
 // startMainPic.part.position.set(2.9, answerButton[5][1].text[2].position.y - 0.62, 0.21);

  
  
  function clearTempGeometries() {
    while (tempGeometry.length > 0) {
      tempGeometry[tempGeometry.length - 1].dispose();
      tempGeometry.pop();
    }
  }
  
  hideLoadingScreen();
}


// прячем экран загузки
function hideLoadingScreen() {
  gsap.to(text_loading.position, { duration: 0.3, y: -10, ease: "power2.in", onComplete: function() {
    scene.remove(text_loading);
    text_loading.geometry.dispose();
  } });
  
  gsap.to(secondBack.material, { duration: 0.6, opacity: 0.0, delay: 0.5, onComplete: function() {
    showStartScreen();
    gsap.to(camera.position, { duration: 0.6, z: 80, ease: "power3.out", delay: 0.4 });
    
    
  } });

  
}

// показываем стартовый экран
function showStartScreen() {
  gsap.from(startMainPic.rotation, { duration: 1.5, x: 6, y: -7, z: 5, ease: "Power4.easeIn" });
  gsap.to(startMainPic.scale, { duration: 1.5, x: 1, y: 1, z: 1, ease: "Power4.easeIn", onComplete: function() {
    gsap.to(startMainPic.scale, { duration: 0.25, x: 1.1, y: 1.1, z: 1.1, ease: "Power1.easeOut", repeat: 1, yoyo: true, onComplete: function() {
    
    } });
    for (let i = 2; i < 26; i++) {
      gsap.to(startMainPic.part[i].scale, { duration: 0.25 + Math.random() * 0.25, x: 1, y: 1, z: 1, ease: "Back.easeOut", delay: Math.random() * 0.25, onComplete: function() {
    
      } });

      
    }
  } });

}



let plusPic = [], resultIcon = [], answerCount, resultButton = [new THREE.Object3D(), new THREE.Object3D(), new THREE.Object3D(), new THREE.Object3D(), new THREE.Object3D()], diagram = new THREE.Object3D(), resultDescription = [new THREE.Object3D(), new THREE.Object3D(), new THREE.Object3D(), new THREE.Object3D(), new THREE.Object3D()], whiteGradient, finText = new THREE.Object3D(), productButton = new THREE.Object3D(), replayButton = new THREE.Object3D(), scaleSprite = (sprite, ratio) => sprite.scale.set(sprite.material.map.image.width * ratio, sprite.material.map.image.height * ratio, 1), socialButton = [], backStuff = new THREE.Object3D(), muscle = new THREE.Object3D(), robot, clown, answerButton = [[], [], [], [], [], [], [], [], [], []], card = [], stepLine = new THREE.Object3D(), startButton = new THREE.Object3D(), answerValue = [[1, 3, 4], [4, 2, 1], [1, 4, 2], [3, 2, 0], [0, 2, 3], [1, 0, 3], [1, 3, 2], [4, 0, 2], [0, 1, 4], [4, 3, 0]], answerGeometry = [[2, 2, 2], [1, 1, 4], [2, 4, 3], [2, 2, 2], [2, 2, 2], [4, 2, 1], [4, 2, 4], [2, 2, 1], [4, 4, 4], [2, 4, 4]];
function prepareGraphics() {
  shape[0] = new THREE.Shape();
  shape[0].absarc(6.5, 1.6, 0.3, 0, Math.PI / 2);
  shape[0].absarc(-6.5, 1.6, 0.3, Math.PI / 2, Math.PI);
  shape[0].absarc(-6.5, -1.6, 0.3, Math.PI, Math.PI * 1.5);
  shape[0].absarc(6.5, -1.6, 0.3, Math.PI * 1.5, Math.PI * 2);
  geometry[1] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.4, curveSegments: 16, bevelEnabled: false }, 4);
  geometry[1].translate(0, 0, -0.2);
  geometry[1].size = 1.9;
  shape[0] = new THREE.Shape();
  shape[0].absarc(6.5, 1.26, 0.3, 0, Math.PI / 2);
  shape[0].absarc(-6.5, 1.26, 0.3, Math.PI / 2, Math.PI);
  shape[0].absarc(-6.5, -1.26, 0.3, Math.PI, Math.PI * 1.5);
  shape[0].absarc(6.5, -1.26, 0.3, Math.PI * 1.5, Math.PI * 2);
  geometry[2] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.4, curveSegments: 16, bevelEnabled: false }, 4);
  geometry[2].translate(0, 0, -0.2);
  geometry[2].size = 1.55;
  shape[0] = new THREE.Shape();
  shape[0].absarc(6.5, 0.56, 0.3, 0, Math.PI / 2);
  shape[0].absarc(-6.5, 0.56, 0.3, Math.PI / 2, Math.PI);
  shape[0].absarc(-6.5, -0.56, 0.3, Math.PI, Math.PI * 1.5);
  shape[0].absarc(6.5, -0.56, 0.3, Math.PI * 1.5, Math.PI * 2);
  geometry[3] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.4, curveSegments: 16, bevelEnabled: false }, 4);
  geometry[3].translate(0, 0, -0.2);
  geometry[3].size = 0.86;
  shape[0] = new THREE.Shape();
  shape[0].absarc(6.5, 0.89, 0.3, 0, Math.PI / 2);
  shape[0].absarc(-6.5, 0.89, 0.3, Math.PI / 2, Math.PI);
  shape[0].absarc(-6.5, -0.89, 0.3, Math.PI, Math.PI * 1.5);
  shape[0].absarc(6.5, -0.89, 0.3, Math.PI * 1.5, Math.PI * 2);
  geometry[4] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.4, curveSegments: 16, bevelEnabled: false }, 4);
  geometry[4].translate(0, 0, -0.2);
  geometry[4].size = 1.19;
  toyName.part1 = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ЧТО ТЫ ЗА', 0.5), 5), whiteBasicMaterial);
  toyName.part1.position.set(-1.9, 0.65, 0);
  toyName.part2 = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ЧЕЛОВЕК', 0.5), 5), whiteBasicMaterial);
  toyName.part2.position.set(-1.9, 0, 0);
  toyName.part3 = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ТАКОЙ?', 0.5), 5), whiteBasicMaterial);
  toyName.part3.position.set(-1.85, -0.65, 0);
  toyName.part1.scale.y = toyName.part1.scale.z = toyName.part2.scale.y = toyName.part2.scale.z = toyName.part3.scale.y = toyName.part3.scale.z = 0;
  toyName.part1.rotation.y = toyName.part2.rotation.y = toyName.part3.rotation.y = Math.PI / 2;
  toyName.add(toyName.part1, toyName.part2, toyName.part3);
  toyName.position.z = 242;
  for (let i = 0; i < 2; i ++) {
    toyDescription[i].text = [];
    for (let j = 0; j < toyDescriptionText[i].length; j ++) {
      toyDescription[i].text[j] = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceRegular.generateShapes(toyDescriptionText[i][j], 0.19), 3), whiteBasicMaterial);
      toyDescription[i].text[j].position.y -= 0.26 * j;
      toyDescription[i].add(toyDescription[i].text[j]);
    }
    toyDescription[i].scale.y = toyDescription[i].scale.z = 0;
    toyDescription[i].rotation.y = Math.PI / 2;
    toyDescription[i].position.set(-2.2, 1.2 - 1.65 * i, 70);
    scene.add(toyDescription[i]);
  }
  shape[0] = new THREE.Shape();
  shape[0].absarc(1.92, 0.24, 0.1, 0, Math.PI / 2);
  shape[0].absarc(-1.92, 0.24, 0.1, Math.PI / 2, Math.PI);
  shape[0].absarc(-1.92, -0.24, 0.1, Math.PI, Math.PI * 1.5);
  shape[0].absarc(1.92, -0.24, 0.1, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.2, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.15);
  startButton.plane = new THREE.Mesh(geometry[0], pinkPurplePhongMaterial);
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('СЕЙЧАС ВЫЯСНЮ!', 0.28), 3);
  geometry[0].computeBoundingBox();
  let xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, -0.11, 0.11);
  startButton.text = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  startButton.add(startButton.plane, startButton.text);
  startButton.scale.y = startButton.scale.z = 0;
  startButton.rotation.y = Math.PI * 1.5;
  startButton.position.set(-0.39, -2.9, 68.9);
  startButton.ready = false;
  stepLine.part = [];
  for (let i = 0; i < 10; i ++) {
    card[i] = new THREE.Object3D();
    card[i].text = [];
    for (let j = 0; j < questionText[i].length; j++) {
      questionText[i][j] = questionText[i][j].toUpperCase();
      geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes(questionText[i][j], 0.52), 3);
      geometry[0].computeBoundingBox();
      let xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
      geometry[0].translate(xMid, -0.95 * j, 0);
      card[i].text[j] = new THREE.Mesh(geometry[0], whiteBasicMaterial);
      card[i].add(card[i].text[j]);
    }
    for (let j = 0; j < 3; j ++) {
      answerButton[i][j] = new THREE.Object3D();
      answerButton[i][j].plane = new THREE.Mesh(geometry[answerGeometry[i][j]], pinkPhongMaterial);
      answerButton[i][j].text = [];
      answerButton[i][j].size = geometry[answerGeometry[i][j]].size;
      for (let k = 0; k < answerText[i][j].length; k++) {
        geometry[0] = new THREE.ShapeBufferGeometry(CirceRegular.generateShapes(answerText[i][j][k], 0.46), 3);
        geometry[0].computeBoundingBox();
        let xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
        geometry[0].translate(xMid, geometry[answerGeometry[i][j]].size - 1 - 0.7 * k, 0.21);
        answerButton[i][j].text[k] = new THREE.Mesh(geometry[0], textBasicMaterial);
        answerButton[i][j].add(answerButton[i][j].text[k]);
      }
      answerButton[i][j].add(answerButton[i][j].plane);
      answerButton[i][j].ready = false;
      scene.add(answerButton[i][j]);
    }
    scene.add(card[i]);
    stepLine.part[i] = new THREE.Mesh(new THREE.CircleBufferGeometry(0.01, 24), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true }));
    stepLine.part[i].position.x = -0.36 + 0.08 * i;
    stepLine.add(stepLine.part[i]);
  }
  stepLine.scale.set(0, 0, 0);
  logo = new THREE.Sprite(pic[11]);
  logo.scale.set(0, 0);
  clown = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.8, 0.8), pic[0]);
  clown.position.set(1.55, answerButton[5][0].text[1].position.y - 0.33, 0.21);
  answerButton[5][0].text[1].position.x -= 0.65;
  answerButton[5][0].plane.add(clown);
  robot = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.8, 0.8), pic[2]);
  robot.position.set(2.9, answerButton[5][1].text[2].position.y - 0.62, 0.21);
  answerButton[5][1].text[2].position.x -= 0.55;
  answerButton[5][1].plane.add(robot);
  muscle.part = [];
  geometry[0] = new THREE.PlaneBufferGeometry(0.74, 0.74);
  for (let i = 0; i < 4; i ++) {
    muscle.part[i] = new THREE.Mesh(geometry[0], pic[1]);
    muscle.part[i].position.x = 0.8 * i;
    muscle.add(muscle.part[i]);
  }
  answerButton[5][2].text[3].position.x -= 1.65;
  muscle.position.set(0.6, answerButton[5][2].text[3].position.y - 1, 0.21);
  answerButton[5][2].plane.add(muscle);
  backStuff.part = [];
  geometry[0] = new THREE.BoxBufferGeometry(22, 22, 22);
  for (let i = 0; i < 17; i++) {
    backStuff.part[i] = new THREE.Mesh(geometry[0], backNormalMaterial);
    gsap.to(backStuff.part[i].rotation, { duration: 15 + Math.random() * 15, x: Math.random() * 6, repeat: -1, yoyo: true, ease: "power1.inOut" });
    gsap.to(backStuff.part[i].rotation, { duration: 15 + Math.random() * 15, y: Math.random() * 6, repeat: -1, yoyo: true, ease: "power1.inOut" });
    gsap.to(backStuff.part[i].rotation, { duration: 15 + Math.random() * 15, z: Math.random() * 6, repeat: -1, yoyo: true, ease: "power1.inOut" });
    backStuff.add(backStuff.part[i]);
  }
  geometry[0] = new THREE.SphereBufferGeometry(19, 36, 48);
  for (let i = 17; i < 31; i++) {
    backStuff.part[i] = new THREE.Mesh(geometry[0], backNormalMaterial);
    backStuff.add(backStuff.part[i]);
  }
  backStuff.part[0].position.set(15, -5, -5);
  backStuff.part[1].position.set(-8, 25, 0);
  backStuff.part[2].position.set(-15, -17, -15);
  backStuff.part[3].position.set(10, -45, 0);
  backStuff.part[4].scale.set(0.8, 0.8, 0.8);
  backStuff.part[4].position.set(27, 17, -20);
  backStuff.part[5].scale.set(0.9, 0.9, 0.9);
  backStuff.part[5].position.set(-23, 58, -20);
  backStuff.part[6].position.set(-45, -17, 0);
  backStuff.part[7].scale.set(0.7, 0.7, 0.7);
  backStuff.part[7].position.set(-41, 40, -20);
  backStuff.part[8].scale.set(0.7, 0.7, 0.7);
  backStuff.part[8].position.set(51, -5, -20);
  backStuff.part[9].position.set(60, 25, 0);
  backStuff.part[10].scale.set(0.9, 0.9, 0.9);
  backStuff.part[10].position.set(60, -17, 0);
  backStuff.part[11].scale.set(0.9, 0.9, 0.9);
  backStuff.part[11].position.set(100, 5, 5);
  backStuff.part[12].scale.set(0.7, 0.7, 0.7);
  backStuff.part[12].position.set(108, -25, -20);
  backStuff.part[13].scale.set(0.7, 0.7, 0.7);
  backStuff.part[13].position.set(-77, -10, 0);
  backStuff.part[14].scale.set(0.6, 0.6, 0.6);
  backStuff.part[14].position.set(-70, 5, -20);
  backStuff.part[15].scale.set(0.8, 0.8, 0.8);
  backStuff.part[15].position.set(-58, 22, 0);
  backStuff.part[16].scale.set(0.9, 0.9, 0.9);
  backStuff.part[16].position.set(-95, 30, 0);
  backStuff.part[17].position.set(25, 45, 0);
  backStuff.part[18].position.set(-27, -50, 0);
  backStuff.part[19].position.set(-42, 9, -30);
  backStuff.part[20].position.set(45, -36, -30);
  backStuff.part[21].scale.set(0.7, 0.7, 0.7);
  backStuff.part[21].position.set(0, -34, -30);
  backStuff.part[22].scale.set(0.4, 0.4, 0.4);
  backStuff.part[22].position.set(-5, 6, -30);
  backStuff.part[23].scale.set(0.5, 0.5, 0.5);
  backStuff.part[23].position.set(38, 15, 9);
  backStuff.part[24].scale.set(0.9, 0.9, 0.9);
  backStuff.part[24].position.set(93, 5, -30);
  backStuff.part[25].position.set(85, -55, -15);
  backStuff.part[26].scale.set(0.5, 0.5, 0.5);
  backStuff.part[26].position.set(108, 35, -20);
  backStuff.part[27].position.set(-95, -45, -20);
  backStuff.part[28].scale.set(0.8, 0.8, 0.8);
  backStuff.part[28].position.set(-115, 10, -30);
  backStuff.part[29].scale.set(0.4, 0.4, 0.4);
  backStuff.part[29].position.set(-130, -20, -30);
  backStuff.part[30].scale.set(0.5, 0.5, 0.6);
  backStuff.part[30].position.set(-90, 50, -30);
  for (let i = 0; i < 3; i ++) {
    socialButton[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[3 + i].map.image.width * 0.008, pic[3 + i].map.image.height * 0.008), pic[3 + i]);
    socialButton[i].position.set(-2 + 2 * i, 708.25, -1100)
    socialButton[i].ready = false;
    scene.add(socialButton[i]);
  }
  shape[0] = new THREE.Shape();
  shape[0].absarc(4.7, 0.75, 0.3, 0, Math.PI / 2);
  shape[0].absarc(-4.7, 0.75, 0.3, Math.PI / 2, Math.PI);
  shape[0].absarc(-4.7, -0.75, 0.3, Math.PI, Math.PI * 1.5);
  shape[0].absarc(4.7, -0.75, 0.3, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.6, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.3);
  productButton.plane = new THREE.Mesh(geometry[0], pinkPurplePhongMaterial);
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ИДУ ИСКАТЬ!', 0.8), 3);
  geometry[0].computeBoundingBox();
  xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, -0.38, 0.31);
  productButton.text = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  productButton.add(productButton.plane, productButton.text);
  productButton.position.set(-1.45, 710.7, -1100);
  productButton.ready = false;
  productButton.scale.y = productButton.scale.z = 0;
  productButton.rotation.y = Math.PI * 1.5;
  shape[0] = new THREE.Shape();
  shape[0].absarc(0.75, 0.75, 0.3, 0, Math.PI / 2);
  shape[0].absarc(-0.75, 0.75, 0.3, Math.PI / 2, Math.PI);
  shape[0].absarc(-0.75, -0.75, 0.3, Math.PI, Math.PI * 1.5);
  shape[0].absarc(0.75, -0.75, 0.3, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.6, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.3);
  replayButton.plane = new THREE.Mesh(geometry[0], purplePhongMaterial);
  replayButton.icon = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.93 * 1.2, 1.04 * 1.2), pic[13]);
  replayButton.icon.position.set(0, 0.05, 0.31);
  replayButton.add(replayButton.plane, replayButton.icon);
  replayButton.position.set(5.35, 710.7, -1100);
  replayButton.ready = false;
  replayButton.scale.x = replayButton.scale.y = 0;
  finText.text = [];
  for (let i = 0; i < finTextText.length; i++) {
    finText.text[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceRegular.generateShapes(finTextText[i], 0.42), 3), green1TextMaterial);
    finText.text[i].position.y = -0.63 * i;
    finText.add(finText.text[i]);
  }
  finText.position.set(-6.5, 715.8, -1100);
  whiteGradient = new THREE.Sprite(pic[6]);
  pic[6].opacity = 0.1;
  whiteGradient.scale.set(100, 7);
  whiteGradient.position.set(0, 720.4, -1100.02);
  geometry[0] = new THREE.CircleBufferGeometry(0.1, 16)
  for (let i = 0; i < 5; i ++) {
    resultDescription[i].whiteText = new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true });
    resultDescription[i].lightGreen = new THREE.MeshBasicMaterial({ color: 0xacfff0, transparent: true });
    resultDescription[i].green = new THREE.MeshBasicMaterial({ color: 0x00ffd1, transparent: true });
    resultDescription[i].description = [];
    for (let j = 0; j < resultDescriptionText[i][0].length; j++) {
      resultDescription[i].description[j] = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceRegular.generateShapes(resultDescriptionText[i][0][j], 0.38), 3), resultDescription[i].whiteText);
      resultDescription[i].description[j].position.y = -0.55 * j;
      resultDescription[i].add(resultDescription[i].description[j]);
    }
    resultDescription[i].skillsTitle = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('Сильные стороны:', 0.4), 3), resultDescription[i].lightGreen);
    resultDescription[i].skillsTitle.position.y = resultDescription[i].description[resultDescriptionText[i][0].length - 1].position.y - 0.95;
    resultDescription[i].jobsTitle1 = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('Подходящие профессии', 0.4), 3), resultDescription[i].lightGreen);
    resultDescription[i].jobsTitle1.position.set(6.75, resultDescription[i].description[resultDescriptionText[i][0].length - 1].position.y - 0.95, 0);
    resultDescription[i].jobsTitle2 = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('(но мы не настаиваем):', 0.4), 3), resultDescription[i].lightGreen);
    resultDescription[i].jobsTitle2.position.set(6.75, resultDescription[i].description[resultDescriptionText[i][0].length - 1].position.y - 1.5, 0);
    resultDescription[i].add(resultDescription[i].skillsTitle, resultDescription[i].jobsTitle1, resultDescription[i].jobsTitle2);
    resultDescription[i].skill = [];
    resultDescription[i].dot = [];
    let skillLine = -resultDescriptionText[i][0].length * 0.55 - 1.28;
    for (let j = 0; j < 3; j ++) {
      resultDescription[i].skill[j] = [];
      resultDescription[i].dot[j] = new THREE.Mesh(geometry[0], resultDescription[i].green);
      for (let k = 0; k < resultDescriptionText[i][1][j].length; k++) {
        resultDescription[i].skill[j][k] = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceRegular.generateShapes(resultDescriptionText[i][1][j][k], 0.38), 3), resultDescription[i].whiteText);
        resultDescription[i].skill[j][k].position.set(0.5, skillLine, 0);
        resultDescription[i].dot[j].position.set(0.1, resultDescription[i].skill[j][0].position.y + 0.13, 0);
        resultDescription[i].add(resultDescription[i].skill[j][k]);
        skillLine -= 0.55;
      }
      skillLine -= 0.2;
      resultDescription[i].add(resultDescription[i].dot[j]);
    }
    resultDescription[i].job = [];
    for (let j = 0; j < 3; j++) {
      resultDescription[i].job[j] = [];
      resultDescription[i].dot[3 + j] = new THREE.Mesh(geometry[0], resultDescription[i].green);
      resultDescription[i].job[j] = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceRegular.generateShapes(resultDescriptionText[i][2][j], 0.38), 3), resultDescription[i].whiteText);
      resultDescription[i].job[j].position.set(7.25, resultDescription[i].jobsTitle2.position.y - 0.9 - 0.75 * j, 0);
      resultDescription[i].dot[3 + j].position.set(6.85, resultDescription[i].job[j].position.y + 0.13, 0);
      resultDescription[i].add(resultDescription[i].job[j], resultDescription[i].dot[3 + j]);
    }
    scene.add(resultDescription[i]);
  }
  resultDescription[0].position.set(-6.5, 722.6, -1100);
  resultDescription[1].position.set(-6.5, 724.3, -1100);
  resultDescription[2].position.set(-6.5, 723.25, -1100);
  resultDescription[3].position.set(-6.5, 724.86, -1100);
  resultDescription[4].position.set(-6.5, 724.86, -1100);
  resultDescription[0].title = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ТЕХНАРЬ', 0.85), 3), green2TextMaterial);
  resultDescription[0].title.geometry.translate(0, 0.85, 0);
  resultDescription[1].title = new THREE.Object3D();
  resultDescription[1].title1 = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ЛИЧНОСТЬ', 0.85), 3), green2TextMaterial);
  resultDescription[1].title2 = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ТВОРЧЕСКАЯ', 0.85), 3), green2TextMaterial);
  resultDescription[1].title1.position.y += 0.85;
  resultDescription[1].title2.position.y += 2;
  resultDescription[1].title.add(resultDescription[1].title1, resultDescription[1].title2);
  resultDescription[2].title = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ДЕЛОВОЙ', 0.85), 3), green2TextMaterial);
  resultDescription[2].title.geometry.translate(0, 0.85, 0);
  resultDescription[3].title = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('АЙТИШНИК', 0.85), 3), green2TextMaterial);
  resultDescription[3].title.geometry.translate(0, 0.85, 0);
  resultDescription[4].title = new THREE.Mesh(new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ГУМАНИТАРИЙ', 0.85), 3), green2TextMaterial);
  resultDescription[4].title.geometry.translate(0, 0.85, 0);
  diagram.part = [];
  diagram.part[0] = new THREE.Mesh(new THREE.CircleBufferGeometry(4.5, 5), new THREE.MeshBasicMaterial({ color: 0xd2cbff, transparent: true, opacity: 0.2 } ));
  diagram.part[1] = new THREE.Mesh(new THREE.CircleBufferGeometry(2.3, 5), new THREE.MeshBasicMaterial({ color: 0xd2cbff, transparent: true, opacity: 0.3} ));
  diagram.part[2] = new THREE.Mesh(new THREE.CircleBufferGeometry(0.4, 5), new THREE.MeshBasicMaterial({ color: 0xd2cbff, transparent: true, opacity: 0.3} ));
  diagram.part[1].position.z = 0.01;
  diagram.part[2].position.z = 0.02;
  diagram.part[18] = new THREE.Mesh(geometry[0], new THREE.MeshBasicMaterial({ color: 0x00ffd1, transparent: true } ));
  diagram.part[18].position.z = 0.06;
  diagram.position.set(3.4, 731.05, -1100);
  diagram.add(diagram.part[0], diagram.part[1], diagram.part[2], diagram.part[18]);
  shape[0] = new THREE.Shape();
  shape[0].absarc(1.36, 0.3, 0.1, 0, Math.PI / 2);
  shape[0].absarc(-1.36, 0.3, 0.1, Math.PI / 2, Math.PI);
  shape[0].absarc(-1.36, -0.3, 0.1, Math.PI, Math.PI * 1.5);
  shape[0].absarc(1.36, -0.3, 0.1, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.2, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.15);
  resultButton[0].plane = new THREE.Mesh(geometry[0], new THREE.MeshPhongMaterial({ color: 0x731f5f, transparent: true }));
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ТЕХНАРЬ', 0.4), 3);
  geometry[0].computeBoundingBox();
  xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, -0.21, 0.11);
  resultButton[0].text = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  resultButton[0].add(resultButton[0].plane, resultButton[0].text);
  resultButton[0].offsetX = 2.1;
  resultButton[0].offsetY = 1.1;
  resultButton[0].rotationPos = 0;
  shape[0] = new THREE.Shape();
  shape[0].absarc(1.9, 0.6, 0.1, 0, Math.PI / 2);
  shape[0].absarc(-1.9, 0.6, 0.1, Math.PI / 2, Math.PI);
  shape[0].absarc(-1.9, -0.6, 0.1, Math.PI, Math.PI * 1.5);
  shape[0].absarc(1.9, -0.6, 0.1, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.2, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.15);
  resultButton[1].plane = new THREE.Mesh(geometry[0], new THREE.MeshPhongMaterial({ color: 0x731f5f, transparent: true }));
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ТВОРЧЕСКАЯ', 0.4), 3);
  geometry[0].computeBoundingBox();
  xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, 0.07, 0.11);
  resultButton[1].text1 = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ЛИЧНОСТЬ', 0.4), 3);
  geometry[0].computeBoundingBox();
  xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, -0.48, 0.11);
  resultButton[1].text2 = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  resultButton[1].add(resultButton[1].plane, resultButton[1].text1, resultButton[1].text2);
  resultButton[1].offsetX = 2.6;
  resultButton[1].offsetY = 1.4;
  resultButton[1].rotationPos = Math.PI / 2.5;
  shape[0] = new THREE.Shape();
  shape[0].absarc(1.55, 0.3, 0.1, 0, Math.PI / 2);
  shape[0].absarc(-1.55, 0.3, 0.1, Math.PI / 2, Math.PI);
  shape[0].absarc(-1.55, -0.3, 0.1, Math.PI, Math.PI * 1.5);
  shape[0].absarc(1.55, -0.3, 0.1, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.2, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.15);
  resultButton[2].plane = new THREE.Mesh(geometry[0], new THREE.MeshPhongMaterial({ color: 0x731f5f, transparent: true }));
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ДЕЛОВОЙ', 0.4), 3);
  geometry[0].computeBoundingBox();
  xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, -0.21, 0.11);
  resultButton[2].text = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  resultButton[2].add(resultButton[2].plane, resultButton[2].text);
  resultButton[2].offsetX = 2.2;
  resultButton[2].offsetY = 1.1;
  resultButton[2].rotationPos = Math.PI / 2.5 * 2;
  shape[0] = new THREE.Shape();
  shape[0].absarc(1.85, 0.3, 0.1, 0, Math.PI / 2);
  shape[0].absarc(-1.85, 0.3, 0.1, Math.PI / 2, Math.PI);
  shape[0].absarc(-1.85, -0.3, 0.1, Math.PI, Math.PI * 1.5);
  shape[0].absarc(1.85, -0.3, 0.1, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.2, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.15);
  resultButton[3].plane = new THREE.Mesh(geometry[0], new THREE.MeshPhongMaterial({ color: 0x731f5f, transparent: true }));
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('АЙТИШНИК', 0.4), 3);
  geometry[0].computeBoundingBox();
  xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, -0.21, 0.11);
  resultButton[3].text = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  resultButton[3].add(resultButton[3].plane, resultButton[3].text);
  resultButton[3].offsetX = 2.6;
  resultButton[3].offsetY = 1.1;
  resultButton[3].rotationPos = Math.PI / 2.5 * 3;
  shape[0] = new THREE.Shape();
  shape[0].absarc(2.3, 0.3, 0.1, 0, Math.PI / 2);
  shape[0].absarc(-2.3, 0.3, 0.1, Math.PI / 2, Math.PI);
  shape[0].absarc(-2.3, -0.3, 0.1, Math.PI, Math.PI * 1.5);
  shape[0].absarc(2.3, -0.3, 0.1, Math.PI * 1.5, Math.PI * 2);
  geometry[0] = new THREE.ExtrudeBufferGeometry(shape[0], { steps: 1, depth: 0.2, curveSegments: 16, bevelEnabled: false });
  geometry[0].translate(0, 0, -0.15);
  resultButton[4].plane = new THREE.Mesh(geometry[0], new THREE.MeshPhongMaterial({ color: 0x731f5f, transparent: true }));
  geometry[0] = new THREE.ShapeBufferGeometry(CirceExtraBold.generateShapes('ГУМАНИТАРИЙ', 0.4), 3);
  geometry[0].computeBoundingBox();
  xMid = -0.5 * (geometry[0].boundingBox.max.x - geometry[0].boundingBox.min.x);
  geometry[0].translate(xMid, -0.21, 0.11);
  resultButton[4].text = new THREE.Mesh(geometry[0], whiteBasicMaterial);
  resultButton[4].add(resultButton[4].plane, resultButton[4].text);
  resultButton[4].offsetX = 2.9;
  resultButton[4].offsetY = 1.1;
  resultButton[4].rotationPos = Math.PI / 2.5 * 4;
  scene.add(resultButton[0], resultButton[1], resultButton[2], resultButton[3], resultButton[4]);
  gsap.to(diagram.rotation, { duration: 0, z: Math.PI / 10, onUpdate: function() {
    for (let i = 0; i < 5; i++) {
      resultButton[i].position.set(diagram.position.x + Math.cos(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetX), diagram.position.y + Math.sin(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetY), -1100);
    }
  } });
  resultIcon[0] = new THREE.Sprite(pic[7]);
  scaleSprite(resultIcon[0], 0.008);
  resultIcon[0].position.set(-3.9, 728.7, -1101);
  resultIcon[1] = new THREE.Sprite(pic[9]);
  scaleSprite(resultIcon[1], 0.005);
  resultIcon[1].position.set(-3.9, 730, -1101);
  resultIcon[2] = new THREE.Sprite(pic[10]);
  scaleSprite(resultIcon[2], 0.0068);
  resultIcon[2].position.set(-3.55, 728.9, -1101);
  resultIcon[3] = new THREE.Sprite(pic[8]);
  scaleSprite(resultIcon[3], 0.0049);
  resultIcon[3].position.set(-4, 729.7, -1101);
  resultIcon[4] = new THREE.Sprite(pic[12]);
  scaleSprite(resultIcon[4], 0.0045);
  resultIcon[4].position.set(-3.8, 729.75, -1101);
  geometry[0] = new THREE.CircleBufferGeometry(0.08, 10);
  geometry[1] = new THREE.PlaneBufferGeometry(4.1, 0.04);
  geometry[2] = new THREE.CircleBufferGeometry(0.1, 10);
  geometry[3] = new THREE.PlaneBufferGeometry(1, 0.04);
  geometry[1].translate(2.05, 0, 0);
  geometry[3].translate(-0.5, 0, 0);
  for (let i = 0; i < 5; i++) {
    resultButton[i].ready = false;
    resultDescription[i].title.position.y = 0.1;
    resultDescription[i].add(resultDescription[i].title);
    diagram.part[i + 3] = new THREE.Mesh(geometry[0], whiteBasicMaterial);
    diagram.part[i + 3].position.set(Math.cos(Math.PI / 2.5 * i) * 4.5, Math.sin(Math.PI / 2.5 * i) * 4.5, 0.01);
    diagram.part[i + 8] = new THREE.Mesh(geometry[0], whiteBasicMaterial);
    diagram.part[i + 8].position.set(Math.cos(Math.PI / 2.5 * i) * 0.4, Math.sin(Math.PI / 2.5 * i) * 0.4, 0.03);
    diagram.part[i + 13] = new THREE.Mesh(geometry[1], whiteBasicMaterial);
    diagram.part[i + 13].position.set(Math.cos(Math.PI / 2.5 * i) * 0.4, Math.sin(Math.PI / 2.5 * i) * 0.4, 0.04);
    diagram.part[i + 13].rotation.z = Math.PI / 2.5 * i;
    diagram.part[i + 19] = new THREE.Mesh(geometry[2], green1TextMaterial);
    diagram.part[i + 19].position.z = 0.05;
    diagram.part[i + 24] = new THREE.Mesh(geometry[3], green1TextMaterial);
    diagram.part[i + 24].position.z = 0.07;
    diagram.part[i + 24].scale.x = 0;
    diagram.add(diagram.part[i + 3], diagram.part[i + 8], diagram.part[i + 13], diagram.part[i + 19], diagram.part[i + 24]);
    scene.add(resultIcon[i]);
  }
  for (let i = 0; i < 6; i++) {
    plusPic[i] = new THREE.Sprite(pic[14 + i]);
    plusPic[i + 6] = new THREE.Sprite(pic[14 + i]);
    plusPic[i + 12] = new THREE.Sprite(pic[14 + i]);
    plusPic[i + 18] = new THREE.Sprite(pic[14 + i]);
    plusPic[i + 24] = new THREE.Sprite(pic[14 + i]);
    plusPic[i + 30] = new THREE.Sprite(pic[14 + i]);
  }
  plusPic[0].position.set(21, 0, 19.9);
  plusPic[1].position.set(10, -27, 19.9);
  plusPic[2].position.set(2, -7, 19.9);
  plusPic[3].position.set(-15, -22, 19.9);
  plusPic[4].position.set(-20, 6, 19.9);
  plusPic[5].position.set(7, 20, 19.9);
  plusPic[6].position.set(-12, 30, 19.9);
  plusPic[7].position.set(31, -19, 19.9);
  plusPic[8].position.set(33, 19, 19.9);
  plusPic[9].position.set(46, -1, 19.9);
  plusPic[10].position.set(55, 27, 19.9);
  plusPic[11].position.set(67, 10, 19.9);
  plusPic[12].position.set(60, -18, 19.9);
  plusPic[13].position.set(-34, -10, 19.9);
  plusPic[14].position.set(-55, -19, 19.9);
  plusPic[15].position.set(-41, 20, 19.9);
  plusPic[16].position.set(-61, 5, 19.9);
  plusPic[17].position.set(-68, 29, 19.9);
  for (let i = 0; i < 18; i++) {
    plusPic[i].material.fog = false;
    scaleSprite(plusPic[i], 0.03);
    backStuff.add(plusPic[i]);
  }
  playLine = 1;
  scene.add(diagram, whiteGradient, finText, replayButton, productButton, backStuff, stepLine, logo, startButton, toyName);
  prepareTest();
}
let startButtonTween, touchReady = true;


// прячем экран загрузки



/*function showStartScreen() {
  gsap.to(text_loading.position, { duration: 0.3, y: -10, ease: "power2.in", onComplete: function() {
    scene.remove(text_loading);
    text_loading.geometry.dispose();
  } });
  gsap.to(secondBack.material, { duration: 0.6, opacity: 0.9, delay: 0.5 });
  gsap.to(toyName.part1.scale, { duration: 0.3, y: 1, z: 1, ease: "power3.out", delay: 0.5 });
  gsap.to(toyName.part1.rotation, { duration: 0.3, y: 0, ease: "back.out", delay: 0.5 });
  gsap.to(toyName.part2.scale, { duration: 0.4, y: 1, z: 1, ease: "power3.out", delay: 0.6 });
  gsap.to(toyName.part2.rotation, { duration: 0.4, y: 0, ease: "back.out", delay: 0.6 });
  gsap.to(toyName.part3.scale, { duration: 0.5, y: 1, z: 1, ease: "power3.out", delay: 0.7 });
  gsap.to(toyName.part3.rotation, { duration: 0.5, y: 0, ease: "back.out", delay: 0.7, onComplete: function() {
    gsap.to(camera.position, { duration: 0.6, z: 80, ease: "power3.out", delay: 0.4 });
    gsap.to(toyName.position, {duration: 0.6, z: 70, ease: "power3.out", delay: 0.4 });
    gsap.to(toyName.part1.position, { duration: 0.3, x: -2.45, y: 3.35, z: -1, ease: "back.in", delay: 0.6 });
    gsap.to(toyName.part2.position, { duration: 0.45, x: -2.45, y: 2.7, z: -1, ease: "back.in", delay: 0.5 });
    gsap.to(toyName.part3.position, { duration: 0.6, x: -2.4, y: 2.05, z: -1, ease: "back.in", delay: 0.4 });
    gsap.to(toyDescription[0].scale, { duration: 0.3, y: 1, z: 1, ease: "power3.out", delay: 1 });
    gsap.to(toyDescription[0].rotation, { duration: 0.3, y: 0, ease: "back.out", delay: 1 });
    gsap.to(toyDescription[1].scale, { duration: 0.4, y: 1, z: 1, ease: "power3.out", delay: 1.1 });
    gsap.to(toyDescription[1].rotation, { duration: 0.4, y: 0, ease: "back.out", delay: 1.1, onComplete: function() {
      gsap.to(startButton.scale, { duration: 0.5, y: 1, z: 1, ease: "back.out" });
      gsap.to(startButton.rotation, { duration: 0.5, y: 0, ease: "back.out", onComplete: function() {
        startButtonTween = gsap.to(startButton.scale, { duration: 0.4, x: 1.02, y: 1.02, ease: "power1.inOut", repeat: -1, yoyo: true });
        document.body.addEventListener('touchstart', onDocumentTouchStart, false);
        document.body.addEventListener('mousedown', onDocumentMouseDown, false);
        document.body.addEventListener('mousemove', onDocumentMouseMove, false);
        startButton.ready = true;
      } });
    } });
  } });
  gsap.to(logo.scale, { duration: 0.4, x: logo.material.map.image.width * 0.0005, y: logo.material.map.image.height * 0.0005, ease: "power2.out" });
}*/
function prepareTest() {
  for (let i = 0; i < 10; i ++) {
    card[i].position.set(0, 60 + 60 * i, -100 * i);
    stepLine.part[i].scale.set(1, 1, 1);
    stepLine.part[i].material.opacity = 0.3;
    let elemPosY = card[i].position.y - questionText[i].length * 0.95 - 0.3;
    for (let j = 0; j < 3; j++) {
      answerButton[i][j].position.set(card[i].position.x, elemPosY - answerButton[i][j].size, card[i].position.z);
      answerButton[i][j].scale.y = answerButton[i][j].scale.z = socialButton[j].material.opacity = 0;
      answerButton[i][j].rotation.x = Math.PI;
      elemPosY -= (answerButton[i][j].size * 2 + 0.4);
    }
  }
  diagram.rotation.z = Math.PI / 10;
  for (let i = 0; i < 5; i ++) {
    whiteGradient.material.opacity = resultButton[i].plane.material.opacity = resultDescription[i].title.scale.y = resultDescription[i].title.scale.z = resultDescription[i].whiteText.opacity = resultDescription[i].lightGreen.opacity = resultDescription[i].green.opacity = 0;
    resultDescription[i].title.rotation.y = Math.PI / 2;
    resultIcon[i].material.opacity = 0;
  }
  finText.scale.y = finText.scale.z = 0;
  finText.rotation.y = Math.PI / 2;
  answerCount = [0, 0, 0, 0, 0];
  showStartScreen();
}
function goPlay() {
  startButtonTween.kill();
  for (let i = 0; i < 2; i++) {
    gsap.to(toyDescription[i].position, { duration: 0.3, x: 15, ease: "power2.in", delay: 0.1 * i });
  }
  gsap.to(toyName.part1.position, { duration: 0.9, x: -30, ease: "power1.in" });
  gsap.to(toyName.part2.position, { duration: 1.35, x: -30, ease: "power1.in" });
  gsap.to(toyName.part3.position, { duration: 1.8, x: -30, ease: "power1.in" });
  gsap.to(toyName.part1.rotation, { duration: 0.3, z: Math.PI / 2, ease: "power1.in" });
  gsap.to(toyName.part2.rotation, { duration: 0.45, z: Math.PI / 2, ease: "power1.in" });
  gsap.to(toyName.part3.rotation, { duration: 0.6, z: Math.PI / 2, ease: "power1.in" });
  gsap.to(startButton.position, { duration: 0.3, z: 67, ease: "circ.out", onComplete: function() {
    gsap.to(stepLine.scale, { duration: 0.4, x: 1.1, y: 1.1, ease: "power2.out", delay: 0.4 });
    goNewCard();
  } });
}
function goNewCard() {
  gsap.to(camera.position, { duration: 0.8, x: card[step].position.x, y: card[step].position.y - (questionText[step].length * 0.95 + answerButton[step][0].size + answerButton[step][1].size + answerButton[step][2].size + 0.9) / 2 - 3.2, z: card[step].position.z + 30, ease: "back.inOut", onComplete: function() {
    for (let i = 0; i < 3; i++) {
      gsap.to(answerButton[step][i].scale, { duration: 0.5, y: 1, z: 1, ease: "back.out", delay: i * 0.2 });
      gsap.to(answerButton[step][i].rotation, { duration: 0.5, x: 0, ease: "back.out", delay: i * 0.2 });
    }
    setTimeout(function() { requestAnimationFrame(function() {
      for (let i = 0; i < 3; i++) {
        answerButton[step][i].ready = true;
      }
    }) }, 800);
  } });
  gsap.to(stepLine.part[step].scale, { duration: 0.8, x: 1.5, y: 1.5, ease: "back.out" });
  gsap.to(stepLine.part[step].material, { duration: 0.8, opacity: 1 });
}
function goFin() {
  playLine = 2;
  goal("final");
  gsap.to(stepLine.scale, { duration: 0.4, x: 0, y: 0, ease: "power2.in" });
  gsap.to(logo.position, { duration: 0.8, y: -1, ease: "power2.in" });
  gsap.to(secondBack.material, { duration: 0.8, opacity: 0.95 });
  gsap.to(logo.scale, { duration: 0.4, x: 0, y: 0, ease: "power2.in" });
  gsap.to(camera, { duration: 1, fov: finView, ease: "back.inOut", onUpdate: function() {
    camera.updateProjectionMatrix();
  } });
  gsap.to(camera.position, { duration: 1, x: diagram.position.x , y: diagram.position.y, z: -1066, ease: "power1.inOut", onComplete: function() {
    for (let i = 0; i < 5; i++) {
      gsap.to(diagram.part[i + 19].position, { duration: 1, x: Math.cos(Math.PI / 2.5 * i) * (4.5 / 6 * answerCount[i]), y: Math.sin(Math.PI / 2.5 * i) * (4.5 / 6 * answerCount[i]), ease: "power2.out" } );
    }
    gsap.to(diagram.part[18].material, { duration: 1, opacity: 0.4, ease: "power1.in", onUpdate: function() {
      shape[0] = new THREE.Shape();
      shape[0].moveTo(diagram.part[19].position.x, diagram.part[19].position.y);
      shape[0].lineTo(diagram.part[20].position.x, diagram.part[20].position.y);
      shape[0].lineTo(diagram.part[21].position.x, diagram.part[21].position.y);
      shape[0].lineTo(diagram.part[22].position.x, diagram.part[22].position.y);
      shape[0].lineTo(diagram.part[23].position.x, diagram.part[23].position.y);
      diagram.part[18].geometry = new THREE.ShapeBufferGeometry(shape[0], 1);
      diagram.part[18].geometry.NeedUpdate = true;
      for (let i = 0; i < 5; i++) {
        diagram.part[i + 24].position.set(diagram.part[i + 19].position.x, diagram.part[i + 19].position.y, 0.06);
        if (diagram.part[i + 19].position.x == 0 && diagram.part[i + 19].position.y == 0) {
          diagram.part[i + 24].rotation.z = Math.PI + Math.PI / 2.5 * (i + 1);
        } else {
          diagram.part[i + 24].rotation.z = - new THREE.Vector3(-diagram.part[i + 19].position.x, -diagram.part[i + 19].position.y, 0).angleTo(new THREE.Vector3(diagram.part[((i + 1) % 5 + 19)].position.x - diagram.part[i + 19].position.x, diagram.part[((i + 1) % 5 + 19)].position.y - diagram.part[i + 19].position.y, 0)) + Math.PI / 2.5 * i;
        }
        diagram.part[i + 24].scale.x = new THREE.Vector3(diagram.part[((i + 1) % 5 + 19)].position.x - diagram.part[i + 19].position.x, diagram.part[((i + 1) % 5 + 19)].position.y - diagram.part[i + 19].position.y, 0).length();
      }
    }, onComplete: function() {
      let checkResult = 0;
      for (let i = 0; i < 5; i++) {
        if (answerCount[i] > checkResult) {
          checkResult = answerCount[i];
          finResult = activeParam = i;
          setShare(i);
        }
      }
      
        if (finResult == 0) {
          goal("result_tech");
        } else if (finResult == 1) {
          goal("result_art");
        } else if (finResult == 2) {
          goal("result_bus");
        } else if (finResult == 3) {
          goal("result_it");
        } else if (finResult == 4) {
          goal("result_gum");
        }
      
      gsap.to(diagram.rotation, { duration: 0.6, z: -Math.PI * 0.78 - resultButton[finResult].rotationPos, onUpdate: function() {
        for (let i = 0; i < 5; i++) {
          resultButton[i].position.set(diagram.position.x + Math.cos(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetX), diagram.position.y + Math.sin(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetY), diagram.position.z);
        }
        
      } });
      gsap.to(camera.position, { duration: 0.6, x: 0, y: finY, z: -1070, ease: "back.inOut", onComplete: function() {
        correctButton();
        gsap.to(whiteGradient.material, { duration: 1.4, opacity: 0.1 });
        gsap.to(resultIcon[activeParam].material, { duration: 0.5, opacity: 1 });
        gsap.to(resultButton[activeParam].rotation, { duration: 0.2, x: Math.PI, ease: "back.in" });
        gsap.to(resultButton[activeParam].scale, { duration: 0.2, x: 0, y: 0, z: 0, ease: "back.in" });
        gsap.to(resultButton[activeParam].position, { duration: 0.2, x: -5, y: resultDescription[finResult].position.y, ease: "power1.in", onComplete: function() {
          gsap.to([resultDescription[activeParam].lightGreen, resultDescription[activeParam].green, resultDescription[activeParam].whiteText], { duration: 0.3, opacity: 1 });
          gsap.to(resultDescription[finResult].title.scale, { duration: 0.3, y: 1, z: 1, ease: "power3.out" });
          gsap.to(resultDescription[finResult].title.rotation, { duration: 0.3, y: 0, ease: "back.out", onComplete: function() {
            for (let i = 0; i < 5; i++) {
              if (i != activeParam) {
                gsap.to(resultButton[i].plane.material, { duration: 0.5, opacity: 1 });
              }
            }
            gsap.to(productButton.scale, { duration: 0.7, y: 0.98, z: 0.98, ease: "back.out" });
            gsap.to(productButton.rotation, { duration: 0.7, y: 0, ease: "back.out", onComplete: function() {
              productButtonTween = gsap.to(productButton.scale, { duration: 0.4, x: 1.02, y: 1.02, ease: "power1.inOut", repeat: -1, yoyo: true });
              for (let i = 0; i < 3; i++) {
                gsap.to(socialButton[i].material, { duration: 0.5, opacity: 1, ease: "bounce.out" });
              }
              gsap.to(replayButton.scale, { duration: 0.5, y: 1, x: 1, ease: "back.out", onComplete: function() {
                replayButton.ready = true;
              } });
            } });
            gsap.to(finText.scale, { duration: 0.3, y: 1, z: 1, ease: "power3.out" });
            gsap.to(finText.rotation, { duration: 0.3, y: 0, ease: "back.out" });
            setTimeout(function() { requestAnimationFrame(function() {
              for (let i = 0; i < 5; i++) {
                resultButton[i].dance = gsap.to(resultButton[i].scale, { duration: 0.3, x: 1.05, y: 1.05, ease: "power1.inOut", repeat: -1, yoyo: true });
                resultButton[i].ready = true;
              }
            }) }, 510);
          } });
        } });
      } });
    } });
  } });
}
function correctButton() {
  if (activeParam == 0) {
    gsap.to(resultButton[1].position, { duration: 0.5, x: resultButton[1].position.x - 1.5, ease: "power1.inOut" });
    gsap.to(resultButton[4].position, { duration: 0.5, x: resultButton[4].position.x - 0.5, y: resultButton[4].position.y - 0.2, ease: "power1.inOut" });
  } else if (activeParam == 1) {
    gsap.to(resultButton[2].position, { duration: 0.5, x: resultButton[2].position.x - 1.1, ease: "power1.inOut" });
    gsap.to(resultButton[0].position, { duration: 0.5, x: resultButton[0].position.x - 0.2, y: resultButton[0].position.y - 0.2, ease: "power1.inOut" });
  } else if (activeParam == 2) {
    gsap.to(resultButton[3].position, { duration: 0.5, x: resultButton[3].position.x - 1.5, ease: "power1.inOut" });
    gsap.to(resultButton[1].position, { duration: 0.5, x: resultButton[1].position.x - 0.4, y: resultButton[1].position.y - 0.1, ease: "power1.inOut" });
  } else if (activeParam == 3) {
    gsap.to(resultButton[4].position, { duration: 0.5, x: resultButton[4].position.x - 1.4, y: resultButton[4].position.y + 0.38, ease: "power1.inOut" });
    gsap.to(resultButton[2].position, { duration: 0.5, x: resultButton[2].position.x - 0.3, y: resultButton[2].position.y - 0.2, ease: "power1.inOut" });
  } else if (activeParam == 4) {
    gsap.to(resultButton[0].position, { duration: 0.5, x: resultButton[0].position.x - 0.3, y: resultButton[0].position.y + 0.38, ease: "power1.inOut" });
    gsap.to(resultButton[3].position, { duration: 0.5, x: resultButton[3].position.x - 0.3, y: resultButton[3].position.y - 0.2, ease: "power1.inOut" });
  }
}
function showParam() {
  gsap.to(diagram.rotation, { duration: 0.6, z: -Math.PI * 0.78 - resultButton[activeParam].rotationPos, onUpdate: function() {
    for (let i = 0; i < 5; i++) {
      resultButton[i].position.set(diagram.position.x + Math.cos(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetX), diagram.position.y + Math.sin(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetY), diagram.position.z);
    }
  }, onComplete: function() {
    correctButton();
    gsap.to(resultButton[activeParam].rotation, { duration: 0.2, x: Math.PI, ease: "back.in" });
    gsap.to(resultIcon[activeParam].material, { duration: 0.5, opacity: 1 });
    gsap.to(resultButton[activeParam].scale, { duration: 0.2, x: 0, y: 0, z: 0, ease: "back.in" });
    gsap.to(resultButton[activeParam].position, { duration: 0.2, x: -5, y: resultDescription[finResult].position.y, ease: "power1.in", onComplete: function() {
      gsap.to([resultDescription[activeParam].lightGreen, resultDescription[activeParam].green, resultDescription[activeParam].whiteText], { duration: 0.3, opacity: 1 });
      gsap.to(resultDescription[activeParam].title.scale, { duration: 0.3, y: 1, z: 1, ease: "power3.out" });
      gsap.to(resultDescription[activeParam].title.rotation, { duration: 0.3, y: 0, ease: "back.out", onComplete: function() {
        for (let i = 0; i < 5; i++) {
          if (i != activeParam) {
            gsap.to(resultButton[i].plane.material, { duration: 0.5, opacity: 1 });
          }
        }
        setTimeout(function() { requestAnimationFrame(function() {
          for (let i = 0; i < 5; i++) {
            resultButton[i].dance = gsap.to(resultButton[i].scale, { duration: 0.4, x: 1.04, y: 1.04, ease: "power1.inOut", repeat: -1, yoyo: true });
            resultButton[i].ready = true;
          }
        }) }, 510);
      } });
    } });
  } });
}
function goReplay() {
  gsap.to(replayButton.position, { duration: 0.3, z: -1103, ease: "circ.out", repeat: 1, yoyo: true, onComplete: function() {
    gsap.to(camera, { duration: 1, fov: view, ease: "back.inOut", onUpdate: function() {
      camera.updateProjectionMatrix();
    } });
    gsap.to(camera.position, { duration: 1, y: 900, z: -1300, ease: "power2.in", onComplete: function() {
      playLine = 1;
      replayButton.scale.set(0, 0, 1);
      productButtonTween.kill();
      productButton.scale.set(1, 0, 0);
      productButton.rotation.y = Math.PI * 1.5;
      gsap.to(secondBack.material, { duration: 0.6, opacity: 0.9, delay: 0.5 });
      gsap.to(logo.scale, { duration: 0.4, x: logo.material.map.image.width * 0.0005, y: logo.material.map.image.height * 0.0005, ease: "power2.out" });
      for (let i = 0; i < 3; i++) {
        socialButton[i].material.opacity = socialButton[i].ready = false;
      }
      for (let i = 0; i < 10; i++) {
        card[i].position.set(0, 60 + 60 * i, -100 * i);
        stepLine.part[i].scale.set(1, 1, 1);
        stepLine.part[i].material.opacity = 0.3;
        let elemPosY = card[i].position.y - questionText[i].length * 0.95 - 0.3;
        for (let j = 0; j < 3; j++) {
          answerButton[i][j].position.set(card[i].position.x, elemPosY - answerButton[i][j].size, card[i].position.z);
          answerButton[i][j].scale.y = answerButton[i][j].scale.z = socialButton[j].material.opacity = 0;
          answerButton[i][j].rotation.x = Math.PI;
          elemPosY -= (answerButton[i][j].size * 2 + 0.4);
        }
      }
      diagram.rotation.z = Math.PI / 10;
      for (let i = 0; i < 5; i++) {
        whiteGradient.material.opacity = resultButton[i].plane.material.opacity = resultDescription[i].title.scale.y = resultDescription[i].title.scale.z = resultDescription[i].whiteText.opacity = resultDescription[i].lightGreen.opacity = resultDescription[i].green.opacity = 0;
        resultDescription[i].title.rotation.y = Math.PI / 2;
        resultButton[i].plane.material.opacity = resultIcon[i].material.opacity = 0;
        resultButton[i].ready = false;
        resultButton[i].dance.kill();
        resultButton[i].rotation.set(0, 0, 0);
        resultButton[i].scale.set(1, 1, 1);
        resultButton[i].position.set(diagram.position.x + Math.cos(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetX), diagram.position.y + Math.sin(diagram.rotation.z + resultButton[i].rotationPos) * (4.5 + resultButton[i].offsetY), diagram.position.z);
      }
      finText.scale.y = finText.scale.z = 0;
      finText.rotation.y = Math.PI / 2;
      answerCount = [0, 0, 0, 0, 0];
      step = 0;
      camera.position.set(0, 0, 50);
      gsap.to(stepLine.scale, { duration: 0.4, x: 1.1, y: 1.1, ease: "power2.out" });
      goNewCard();
    } });
  } });
}
const raycaster = new THREE.Raycaster(), mouse = new THREE.Vector2();
function onDocumentMouseDown(event) {
  event.preventDefault();
  touchReady = false;
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, camera);
  checkInteractive(raycaster);
}
function onDocumentTouchStart(event) {
  touchDetect = true;
  event.preventDefault();
  if (touchReady) {
    mouse.x = (event.touches[0].clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.touches[0].clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(mouse, camera);
    checkInteractive(raycaster);
  }
}
function checkInteractive(raycaster) {
  intersects = raycaster.intersectObject(startButton.plane);
  if (intersects.length > 0 && startButton.ready) {
    goal("btn_start");
    startButton.ready = false;
    goPlay();
  }
  intersects = raycaster.intersectObject(productButton.plane);
  if (intersects.length > 0) {
    goal("btn_product");
    window.open("https://vk.com/prof.uchi");
  }
  intersects = raycaster.intersectObject(replayButton.plane);
  if (intersects.length > 0 && replayButton.ready) {
    goal("btn_restart");
    replayButton.ready = productButton.ready = false;
    goReplay();
  }
  for (let i = 0; i < 3; i++) {
    intersects = raycaster.intersectObject(answerButton[step][i].plane);
    if (intersects.length > 0 && answerButton[step][i].ready) {
      answerCount[answerValue[step][i]] ++;
      for (let j = 0; j < 3; j++) {
        answerButton[step][j].ready = false;
        if (j != i) {
          gsap.to(answerButton[step][j].position, { duration: 0.3, y: answerButton[step][i].position.y - 30, ease: "power1.in" } );
        }
      }
      if (step == 2) {
        goal("progress_3");
      } else if (step == 5) {
        goal("progress_6");
      } else if (step == 8) {
        goal("progress_9");
      }
      gsap.to(answerButton[step][i].position, { duration: 0.5, y: card[step].position.y - questionText[step].length * 0.95 - 0.3 - answerButton[step][i].size, ease: "circ.inOut" } );
      gsap.to(answerButton[step][i].position, { duration: 0.25, z: answerButton[step][i].position.z - 10, ease: "circ.out", repeat: 1, yoyo: true });
      gsap.to(answerButton[step][i].rotation, { duration: 0.5, x: -Math.PI * 2, ease: "power1.inOut" });
      gsap.to(camera.position, { duration: 0.5, y: card[step].position.y - (questionText[step].length * 0.95 + answerButton[step][i].size + 0.3) / 2 - 1, ease: "Power2.inOut" } );
      setTimeout(function() { requestAnimationFrame(function() {
        if (step < 9) {
          step ++;
          goNewCard();
        } else {
          goFin();
        }
      }) }, 800);
    }
    intersects = raycaster.intersectObject(socialButton[i]);
    if (intersects.length > 0) {
      if (i == 0) {
        share.open('tw');
      } else if (i == 1) {
        share.open('fb');
      } else {
        share.open('vk');
      }
    }
  }
  for (let i = 0; i < 5; i++) {
    intersects = raycaster.intersectObject(resultButton[i].plane);
    if (intersects.length > 0 && resultButton[i].ready) {
      goal("web_click")
      let next = i;
      i = 5;
      for (let j = 0; j < 5; j++) {
        resultButton[j].dance.kill();
        resultButton[j].scale.set(1, 1, 1);
        resultButton[j].ready = false;
        gsap.to(resultButton[j].position, { duration: 0.3, x: diagram.position.x + Math.cos(diagram.rotation.z + resultButton[j].rotationPos) * (4.5 + resultButton[j].offsetX), y: diagram.position.y + Math.sin(diagram.rotation.z + resultButton[j].rotationPos) * (4.5 + resultButton[j].offsetY), ease: "power1.inOut" } );
        gsap.to(resultButton[j].plane.material, { duration: 0.3, opacity: 0 });
      }
      gsap.to(resultIcon[activeParam].material, { duration: 0.3, opacity: 0, ease: "bounce.out" });
      gsap.to([resultDescription[activeParam].lightGreen, resultDescription[activeParam].green, resultDescription[activeParam].whiteText], { duration: 0.6, opacity: 0 });
      gsap.to(resultDescription[activeParam].title.scale, { duration: 0.3, y: 0, z: 0, ease: "power3.in" });
      gsap.to(resultDescription[activeParam].title.rotation, { duration: 0.3, y: Math.PI / 2, ease: "back.in" });
      gsap.to(resultButton[activeParam].rotation, { duration: 0.3, x: 0, ease: "back.out" });
      gsap.to(resultButton[activeParam].scale, { duration: 0.3, x: 1, y: 1, z: 1, ease: "back.in", onComplete: function() {
        activeParam = next;
        showParam();
      } });
    }
  }
  touchReady = true;
}
function onDocumentMouseMove(event) {
  if (!touchDetect) {
    event.preventDefault();
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(mouse, camera);
    document.body.style.cursor = "default";
    intersects = raycaster.intersectObject(startButton.plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
    }
    intersects = raycaster.intersectObject(productButton.plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
    }
    intersects = raycaster.intersectObject(replayButton.plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
    }
    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 3; j++) {
        intersects = raycaster.intersectObject(answerButton[i][j].plane);
        if (intersects.length > 0) {
          document.body.style.cursor = "pointer";
        }
      }
    }
    for (let i = 0; i < 5; i++) {
      intersects = raycaster.intersectObject(resultIcon[i]);
      if (intersects.length > 0) {
        document.body.style.cursor = "pointer";
      }
    }
    for (let i = 0; i < 5; i++) {
      intersects = raycaster.intersectObject(resultButton[i].plane);
      if (intersects.length > 0) {
        document.body.style.cursor = "pointer";
      }
    }
  }
}
function onWindowResize() {
  if (playLine < 2) {
    if (window.innerHeight / window.innerWidth > 1.572) {
      view = (Math.atan((window.innerHeight / 2) / ((window.innerWidth / 2) / Math.tan(0.26533)))) * 360 / Math.PI;
    } else {
      view = 46;
    }
  } else {
    if (window.innerHeight / window.innerWidth > 1.71) {
      view = 52;
      finView = 52;
      finY = 721;
    } else {
      view = 45;
      finView = 45;
      finY = 719.5;
    }
    camera.position.y = finY;
  }
  if (window.innerHeight / window.innerWidth > 1.71) {
    finView = 52;
    finY = 721;
  } else {
    finView = 45;
    finY = 719.5;
  }
  camera.fov = view;
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}
loop();
function loop() {
  secondBack.position.set(camera.position.x, camera.position.y, camera.position.z - 71);
  if (playLine > 0) {
    logo.position.set(camera.position.x, camera.position.y - 0.78 - (camera.fov - 46) * 0.02, camera.position.z - 2);
    stepLine.position.set(camera.position.x, camera.position.y - 0.65, camera.position.z - 2);
    backStuff.position.set(camera.position.x, camera.position.y, camera.position.z - 91);
  }
  renderer.render(scene, camera);
  requestAnimationFrame(loop);
}