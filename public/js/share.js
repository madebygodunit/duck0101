class Share {
  /**
   * Only one object is passing to constructor
   * Params:
   *        url         {String} url to share
   *        title       {String} site title
   *        image       {String} image url
   *        description {String} site description
   *        metrics     {Object} yandex metrics object (?)
   *        prefix      {String} prefix for metrics goal
   * @param {Object} params
   */
  constructor(params) {
    // assign params to context
    Object.assign(this, params);
    // set default prefix if not passed.
    // Prefix for metrics goal prefix+name (ex. share_vk)
    this.prefix = params.prefix || 'share_';
    // popup params
    this.width = 600;
    this.height = 600;
    // default value for canvas fix
    this.isCanvas = false;
    // default value for window open mode
    this.mode = '';
    // library
    this.library = 'vanilla';
  }

  /**
   * sites data for generating link
   * @return {Object}
   */
  get sites() {
    let list = {
      'vk': {
        'domain': 'http://vk.com/share.php?',
        'params': {
          'url': this.url,
          'title': this.title,
          'description': this.description,
        },
      },
      'fb': {
        'domain': 'http://www.facebook.com/sharer.php?',
        'params': {
          'u' : this.url,
        }
      },
      'tw': {
        'domain': 'http://twitter.com/share?',
        'params': {
          'text': this.description,
          'url': this.url,
        }
      },
      'ok': {
        'domain': 'https://connect.ok.ru/offer?',
        'params': {
          'url' : this.url,
          'title' : this.title,
          'imageUrl' : this.image,
        }
      },
    };

    return list;
  }

  reachGoal(name) {
    switch (this.library) {
      case 'vanilla':
        this.metrics(this.id, 'reachGoal', `${this.prefix}${name}`);
        break;
      case 'vue':
        this.metrics.reachGoal(`${this.prefix}${name}`);
        break;
      case 'react':
        this.metrics('reachGoal', `${this.prefix}${name}`);
        break;
    }
  }

  /**
   * check if data for this site available
   * @param  {String} name site short name
   * @return {Object}      object with site data
   */
  check(name) {
    let site = this.sites[name];

    if (!site || typeof site === 'undefined') {
      throw new Error(`Wrong site (${name}). Available: ${Object.keys(this.sites).join(',')}`);
    }

    if (this.library === 'vanilla' && !this.id) {
      throw new Error(`Choosen 'vanilla', please pass 'id'`);
    }

    const libs = ['vanilla', 'vue', 'react'];

    if (!libs.includes(this.library)) {
      throw new Error(`Wrong library (${this.library}. Available: ${Object.keys(libs).join(',')})`);
    }

    return site;
  }

  /**
   * generating final share link
   * you can use it like Share.create('vk')
   * ⚠️ it only generate link, no metrics call
   * @param  {String} name site short name
   * @return {String}      final share link
   */
  create(name) {
    // check if valid site passed first
    let site = this.check(name);

    // generate params from object
    let params = Object.entries(site.params)
      .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
      .join('&');

    // join domain and params
    let shareLink = `${site.domain}${params}`;

    return shareLink;
  }

  /**
   * open share link in popup and call metrics
   * @param  {String} name site short name
   */
  open(name) {
    // check if valid site passed first
    this.check(name);

    // call metrics if ya.metrics object(?) passed
    if (this.metrics) {
      this.reachGoal(name);
    }
    // open popup
    if (this.isCanvas) {
      function OpenURL (url) {
        let gameCanvas = document.getElementsByTagName('canvas')[0];

        if (gameCanvas !== null)  {
          let endInteractFunction = function() {
            window.open(url, this.mode, this.mode === '' ? `width=${this.width},height=${this.height}` : '');
            gameCanvas.onmouseup = null;
            gameCanvas.ontouchend = null;
          }

          gameCanvas.ontouchend = endInteractFunction;
          gameCanvas.onmouseup = endInteractFunction;
        }
      }

      OpenURL(this.create(name));
    } else {
      window.open(this.create(name), this.mode, this.mode === '' ? `width=${this.width},height=${this.height}` : '');
    }
  }
}
